<?php

namespace App\Http\Controllers;

use SimpleXMLElement;
use Illuminate\Http\Request;

class WebhookController extends Controller
{
    
    public function index(Request $request){
        $payload = json_decode($request->getContent(), true); 

        $order_line_items = $payload['line_items'];

        // Creating array with order values. Some values are not coming in order, so keeping them blank.
        $order_data = array (
            'ORDERS' => 
            array (
                'BUYER' => '',
                'CURRENCY_CODE' => $payload['currency'],
                'CUSTOMER_COMPANY_COUNTRY' => '',
                'EAN_LOCATION_DELIVERY_ADDRESS' => $payload['shipping_address']['address1'].', '.$payload['shipping_address']['address2'],
                'EAN_SUPPLIER_DOCUMENT_ADDRESS' => '',
                'ORDER_DATE' => $payload['created_at'],
                'OUR_CUSTOMER_NO' => $payload['id'],
                'RECEIVER_COMMUNICATION_ID' => '',
                'VENDOR_NO' => '',
                'WANTED_DELIVERY_DATE' => '',
                'CONTRACT' => '',
                'SENDER_COMMUNICATION_ID' => '',
                'CUSTOMER_PO_NO' => $payload['order_number'],
                'LANGUAGE_CODE' => $payload['client_details']['accept_language'],
                'EAN_LOCATION_PAYER_ADDRESS' => $payload['billing_address']['address1'].', '.$payload['billing_address']['address2'],
                'DELIVER_TO' => 
                array (
                    'ORDER_ADDRESS' => 
                    array (
                        'RECEIVER_NAME' => $payload['customer']['first_name'].' '.$payload['customer']['last_name'],
                        'ADDRESS_1' => $payload['customer']['default_address']['address1'],
                        'COUNTRY_CODE' => $payload['customer']['default_address']['country_code'],
                    ),
                ),
                'ORDER_LINES' => 
                array (
                    'ORDER_LINE' => $order_line_items,
                ),
                '@attributes' => 
                array (
                    'schemaLocation' => 'urn:ifsworld-com:schemas:receive_customer_order',
                ),
            ),
        );
        
        $xml_data = new SimpleXMLElement('<?xml version="1.0"?><ORDERS></ORDERS>');
        $this->array_to_xml($order_data,$xml_data);
        // print $xml_data->asXML(); die;
        
        $file_name = 'order_'.$payload['id'].'.xml';
        $result = $xml_data->asXML(base_path('/public/order-xml/'.$file_name)); // File will be stored inside /public/order-xml folder

        $source_file = base_path('/public/order-xml/'.$file_name);

        //  Getting configuration values from ENV file.
        $host = env("WEBHOOK_HOST", "default_host");
        $user = env("WEBHOOK_USERNAME", "default_username");
        $pass = env("WEBHOOK_PASSWORD", "default_password");

        // Establishing FTP connection
        $ftp = ftp_connect($host) or die('Error connecting to ftp server...');

        // FTP Login
        $connect = ftp_login($ftp, $user, $pass);

        ftp_pasv($ftp, true);

        // UPloading XML file to its destination
        $ret = ftp_nb_put($ftp, "/public_html/projects/demo/".$file_name, $source_file, FTP_ASCII); // public_html/projects/demo should be replaceed with destination path
        
        while (FTP_MOREDATA == $ret)
        {
        // display progress bar, or something
            $ret = ftp_nb_continue($ftp);
        }

    }

    public function array_to_xml( $data, &$xml_data ) {
        foreach( $data as $key => $value ) {
            if( is_array($value) ) {
                if( is_numeric($key) ){
                $key = 'item'.$key; //dealing with <0/>..<n/> issues
            }
            $subnode = $xml_data->addChild($key);
            $this->array_to_xml($value, $subnode);
        } else {
            $xml_data->addChild("$key",htmlspecialchars("$value"));
        }
    }
}

}
